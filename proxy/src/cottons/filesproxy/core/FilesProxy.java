package cottons.filesproxy.core;

import cotton.restful.RestfulClient;
import cotton.restful.RestfulClient.RestfulSecureConnection;
import cotton.restful.core.exceptions.RestfulFailure;
import cottons.filesproxy.FilesApi;
import cottons.filesproxy.exceptions.FilesApiFailure;

import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class FilesProxy implements FilesApi {
    private RestfulClient client;

    @Override
    public Connection connect(String url) {
        return connect(url, null, null);
    }

    @Override
    public Connection connect(String url, URL certificate, String password) {
        return new Connection() {
            private RestfulSecureConnection connection = null;

            @Override
            public void upload(File... files) throws FilesApiFailure {
                try {
                    secure().postFiles("/files", filesToMap(files));
                } catch (RestfulFailure error) {
                    throw new FilesApiFailure(error.getMessage());
                }
            }

            @Override
            public InputStream download(String id) throws FilesApiFailure {
                try {
                    return secure().getFile(String.format("/file/%s", id));
                } catch (RestfulFailure error) {
                    throw new FilesApiFailure(error.getMessage());
                }
            }

            private RestfulSecureConnection secure() throws FilesApiFailure {
                try {
                    if (connection == null)
                        connection = client.secureConnection(new URL(url), certificate, password);
                    return connection;
                } catch (MalformedURLException error) {
                    throw new FilesApiFailure(error.getMessage());
                }
            }

            private Map<String, InputStream> filesToMap(File[] files) {
                Map<String, InputStream> result = new HashMap<>();
                for (File file : files) {
                    result.put(file.id(), file.content());
                }
                return result;
            }
        };
    }

}
