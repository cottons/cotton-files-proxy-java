package cottons.filesproxy.core;

import java.io.InputStream;

public class File {
    private final String id;
    private final InputStream content;

    public File(String id, InputStream content) {
        this.id = id;
        this.content = content;
    }

    public String id() {
        return id;
    }

    public InputStream content() {
        return content;
    }
}
