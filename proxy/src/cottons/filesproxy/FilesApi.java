package cottons.filesproxy;

import cottons.filesproxy.core.File;
import cottons.filesproxy.exceptions.FilesApiFailure;

import java.io.InputStream;
import java.net.URL;

public interface FilesApi {
    Connection connect(String url);
    Connection connect(String url, URL certificate, String password);

    interface Connection {
        void upload(File... files) throws FilesApiFailure;
        InputStream download(String id) throws FilesApiFailure;
    }
}
